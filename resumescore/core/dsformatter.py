import os

import numpy as np
import pandas as pd
import nltk
from nltk import Tree
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tag.stanford import StanfordNERTagger

from gensim.models import Word2Vec

from resumescore import config
from resumeparser import config as resume_config
from common import config as common_config
from resumeparser.core.namedentityrecognizer import StanfordNER


class DataFormatter:

    def __init__(self):
        self.model = Word2Vec.load(os.path.join
                                   (config.ROOT,
                                    'models/word2vec'
                                    )
                                   )
        jar = os.path.join(common_config.ROOT, 'stanford-ner-tagger/stanford-ner.jar')
        self.model_academics = os.path.join(common_config.ROOT, 'stanford-ner-tagger/resume.ser.gz')
        self.ner_tagger_academics = StanfordNERTagger(self.model_academics, jar, encoding='utf8')

    def prepare_profile_data(self,profile):
        skills,education,experience = profile['skills'],\
                                      [education_dict['degree'] for education_dict in profile['education']],\
                                      profile['tech_exp']

        document = skills + experience
        document = ' '.join(document)
        for data in profile["education"]:
            document += ' ' + str(data.get('degree')) + ' ' + str(data.get('program')) + ' '
        for data in profile['work']:
            document+= str(data['job_title']) + ' ' + str(data['organization']) + ' ' + str(
                data['responsibilities']) + ' '

        return (skills, education, experience,document)

    def preparecv4score(self,profiledocument):
        cv_word2vec = []
        profile_tokens = profiledocument.split()
        profile_word2vec = [self.model.wv[profile_token] for profile_token in profile_tokens if profile_token in self.model.wv]
        cv_word2vec.append(((np.mean(profile_word2vec,axis = 0)),'score'))
        return cv_word2vec

    def preparejob4score(self,job):
        data = job.get('responsibilities') + ' ' + \
                job.get('skills_qualification') + ' ' + \
                job.get('description')

        return data,job['job_pk']

    def skills_extractor(self,jobdescription):
        chunked_phrases = []
        chunk_noun_phrase = "noun_phrase:{<NNP|NN|NNS><IN>?<NNP|NN>*[-|:]?<CD>?}"
        cp = nltk.RegexpParser(chunk_noun_phrase)
        tokens = word_tokenize(jobdescription)
        tagged_tokens = nltk.pos_tag(tokens)
        if tagged_tokens:
            chunked_noun_phrase = cp.parse(tagged_tokens)
            for subtree in chunked_noun_phrase:
                if type(subtree) == Tree and subtree.label() == "noun_phrase":
                    chunked_phrases.append(" ".join([token for token, pos in subtree.leaves()]))
        noun_chunks = list(set(chunked_phrases))
        tokenized_words = word_tokenize(jobdescription)
        tokens = [token for token in tokenized_words if token not in stopwords.words('english')]
        data = pd.read_csv(os.path.join(resume_config.ROOT, 'staticfiles/skills.csv'))
        skills = list(data.columns.values)
        skillset = []
        # checking for the one-grams
        for token in tokens:
            if token.lower() in skills:
                skillset.append(token)

        # check for bi-grams and tri-grams
        for token in noun_chunks:
            token = token.lower().strip()
            if token in skills:
                skillset.append(token)
        skillset = [i.capitalize() for i in set([i.lower() for i in skillset])]
        return (skillset)

    # This function here returns academics in listed form
    def education_extractor(self, jobdescription):
        listed_education = []
        if jobdescription:
            academics = StanfordNER.ner_parser(self.ner_tagger_academics, jobdescription, "academics")
            for key, value in academics.items():
                listed_education.append(value["Degree"])
        return listed_education

    # This function here returns experience in listed format
    def experience_chunker(self, jobdescription):
        tokens = word_tokenize(jobdescription)

        tagged_tokens = nltk.pos_tag(tokens)
        chunked_phrases_a = []
        chunked_phrases_b = []
        chunked_phrases_c = []
        chunk_rule_a = "experience_type_a:{<CD><N.+><IN><J.+|V.+>?<N.+><IN>?<N.+>+}"
        chunk_rule_b = "experience_type_b:{<N.+>+<\(>*<CD><N.+><N.+>?<IN>?<J.+|VBG>?<N.+>?<\)>*}"
        chunk_rule_c = "experience_type_c:{<N.+>+<\:>*<CD><N.+><N.+>?<IN>?<J.+|VBG>?<N.+>?<N.+>*?}"
        cpa = nltk.RegexpParser(chunk_rule_a)
        cpb = nltk.RegexpParser(chunk_rule_b)
        cpc = nltk.RegexpParser(chunk_rule_c)
        if tagged_tokens:
            exp_a = cpa.parse(tagged_tokens)
            exp_b = cpb.parse(tagged_tokens)
            exp_c = cpc.parse(tagged_tokens)
            for subtree in exp_a:
                if type(subtree) == nltk.Tree and subtree.label() == "experience_type_a":
                    chunked_phrases_a.append(" ".join([token for token, pos in subtree.leaves()]))
            for subtree in exp_b:
                if type(subtree) == nltk.Tree and subtree.label() == "experience_type_b":
                    chunked_phrases_b.append(" ".join([token for token, pos in subtree.leaves()]))
            for subtree in exp_c:
                if type(subtree) == nltk.Tree and subtree.label() == "experience_type_c":
                    chunked_phrases_c.append(" ".join([token for token, pos in subtree.leaves()]))
        chunked_phrases_a.extend(chunked_phrases_b)
        chunked_phrases_a.extend(chunked_phrases_c)
        return (chunked_phrases_a)



