import math
import random
import numpy as np
from scipy import spatial
from resumescore.core.dsformatter import  DataFormatter
from resumeparser.core.similaritycalculator import *

class ScoreGenerator(DataFormatter):

    def keywordmatcher(self, jobdescription, profile_data):
        out_put = {'skills': {}, 'Education': {}, 'Experience': {}}
        jobdescription = str(jobdescription['description']) + str(jobdescription['skills_qualification']) + str(
            jobdescription['responsibilities'])
        skills, education, experience,_ = self.prepare_profile_data(profile_data)
        formatted_skills = [w.lower() for w in skills]

        for skill in self.skills_extractor(jobdescription):
            if skill.lower() in formatted_skills:
                out_put['skills'].update({skill: True})
            else:
                out_put['skills'].update({skill: False})

        for education in self.education_extractor(jobdescription):
            flag = 0
            for edu in education:
                if sentence_similarity(education, edu) >= 0.85 and flag == 0:
                    out_put['Education'].update({education: True})
                    flag = 1
                else:
                    pass
            if flag == 0:
                out_put['Education'].update({education: False})
            else:
                pass

        for experience in self.experience_chunker(jobdescription):
            flag = 0

            for exp in experience:
                if sentence_similarity(experience, exp) >= 0.85 and flag == 0:
                    out_put['Experience'].update({experience: True})
                    flag = 1
                else:
                    pass
            if flag == 0:
                out_put['Experience'].update({experience: False})
            else:
                pass

        return out_put

    def rulebased_scorer(self,job_skill,job_educt,job_xp,skills,education,experience):
        score_skills = 20
        score_education = 10
        score_experience = 5
        if len(job_skill) > 0:
            score_skills =  len([w for w in skills if w in job_skill])/len(job_skill) * 50

        if len(job_educt) > 0:
            score_education = len([edu for edu in education
                                   for educt in job_educt
                                   if sentence_similarity(educt,edu)>=0.85])/\
                                    len(job_educt) * 20

        if len(job_educt) > 0:
            score_experience = len([xp for xp in experience
                                    for exp in job_xp
                                    if sentence_similarity(exp,xp)>=0.85])\
                                    /len(job_xp) * 30

        final_score = score_skills + score_education + score_experience

        return final_score

    def generate_score(self,jobdescription,profile_data):
        default_score = [20]
        skills,education,experience,document = self.prepare_profile_data(profile_data)
        cv_word2vec = self.preparecv4score(document)
        prepared_jobs = map(self.preparejob4score,jobdescription)
        job = []
        for doc,pk in prepared_jobs:
            job_word2vec = []
            job_skill = self.skills_extractor(doc)
            job_educt = self.education_extractor(doc)
            job_xp = self.experience_chunker(doc)
            necessary_docs = ' '.join(job_skill +  job_educt + job_xp)
            necessary_tokens = necessary_docs.split()
            rulebased_score = self.rulebased_scorer(job_skill,job_educt,job_xp,skills,education,experience)
            # matched_skills
            for token in necessary_tokens:
                try:
                    job_word2vec.append(self.model.wv[token])
                except:
                    try:
                        word_small = token.lower()
                        job_word2vec.append(self.model[word_small])
                    except Exception as e:
                        print(e)
                        pass
            job_word2vec = np.mean(job_word2vec, axis=0)
            # use cosine similarity to measure the score.
            retrieval = []
            for i in range(len(cv_word2vec)):
                retrieval.append((1 - spatial.distance.cosine(job_word2vec, cv_word2vec[i][0]), cv_word2vec[i][1]))

            retrieval.sort(reverse=True)

            for score in retrieval:

                if score[0] < 0 or math.isnan((score[0])):
                    job.append(
                        {'pk': pk, 'score': (math.floor(random.choice(default_score)) * 0.7 + rulebased_score * 0.3)})
                elif score[0] > 100:
                    job.append(
                        {'pk': pk, 'score': (90)})
                else:
                    print(score)
                    job.append({'pk': pk, 'score': (math.floor(score[0] * 100 * 0.7 + rulebased_score * 0.3))})
        return job






