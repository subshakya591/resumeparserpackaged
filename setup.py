from setuptools import setup

setup(name='resumeparser',
      version='0.9.5',
      description='Resume parser Packaged',
      url='https://gitlab.com/subshakya591/resumeparserpackaged.git',
      author='Subodh Chandra Shakya',
      author_email='subshakya591@gmail.com',
      license='Info developers',
      packages=['resumeparser'],
      zip_safe=False,
      include_package_data=True)

#2384370