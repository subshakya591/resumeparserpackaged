import datetime

from py2neo import Graph, Node, Relationship

from resumeparser.scoregenerator import ScoreGenerator


class GraphDatabase:

    def __init__(self):
        print("entering the graph database")
        # Use your NEO4J database password
        self.graph = Graph('bolt://silver-cathryn-island-ruth.graphstory.cloud:7687', auth=('silver_cathryn_island_ruth', 'IGyKoMerrcjVwN4CeJxJ8uqLjo7Me'))
        self.tech_skills = Node("TechnicalSkills")
        self.non_tech_skills = Node("NonTechnicalSkills")


    def populate_profile(self, profile_data):
        dict_data_personal = profile_data['personal_data']
        person_id = dict_data_personal['id']
        first_name = dict_data_personal['first_name']
        middle_name = dict_data_personal['middle_name']
        last_name = dict_data_personal['last_name']
        gender = dict_data_personal['gender']
        technical_skill = profile_data['skills']
        # non_technical_skills=dict_data['']
        company = {"companies": []}
        for exp in profile_data['work']:
            item = {}
            item.update(
                {"name": exp['organization'],
                 "designation": exp['job_title'],
                 "location": exp['location']
                 }
            )
            company['companies'].append(item)

        education = {"institutes": []}

        for edu in profile_data['education']:
            item = {}
            item.update(
                {"name": edu['institution'],
                 "degree": edu['degree'],
                 "program": edu['program'],
                 "location": edu['location']
                 }
            )
            education['institutes'].append(item)

        '''
        Create a unique contrain on user with unique user id
        '''
        self.graph.run('''CREATE CONSTRAINT ON (p:Person)
            ASSERT p.person_id IS UNIQUE
            ''')

        person = Node("Person")
        person['person_id'] = person_id
        person['first_name'] = first_name
        person['middle_name'] = middle_name
        person['last_name'] = last_name
        person['gender'] = gender
        self.graph.merge(person, "Person", 'first_name')

        self.tech_skills['type'] = "Technical"
        self.graph.merge(self.tech_skills, "TechnicalSkills", "type")

        for i in technical_skill:
            t_skills = Node("Technical")
            t_skills['skills'] = i
            self.graph.merge(t_skills, 'Technical', 'skills')
            user_technical_skills = Relationship(person, "HAS_SKILL", t_skills)
            self.graph.merge(user_technical_skills)
            technical_skills = Relationship(t_skills, "IS", self.tech_skills)
            self.graph.merge(technical_skills)

        self.non_tech_skills['name'] = "NonTechnical"
        # self.graph.merge(non_tech_skills, "NonTechnicalSkills", "name")
        #
        # for i in non_technical_skill:
        #     n_skills = Node('NonTechnical')
        #     n_skills['skills'] = i
        #     graph.merge(n_skills, "NonTechnical", 'skills')
        #     user_non_technical_skills = Relationship(person, "HAS_SKILL", n_skills)
        #     graph.merge(user_non_technical_skills)
        #     non_technical_skills = Relationship(n_skills, "IS", non_tech_skills)
        #     graph.merge(non_technical_skills)

        for i in company['companies']:
            work_details = Node('Work')
            work_details['company'] = i['name']
            work_details['designation'] = i['designation']
            self.graph.merge(work_details, "Work", "company")
            work_address = Node('CompanyLocation')
            work_address['company_location'] = i['location']
            self.graph.merge(work_address, "CompanyLocation", 'company_location')
            user_work = Relationship(person, "WORKED_IN", work_details)
            work_place = Relationship(work_details, "IS_IN", work_address)
            self.graph.create(user_work)
            self.graph.create(work_place)

        for i in education['institutes']:
            college = Node('Institute')
            college['name'] = i['name']
            college['degree'] = i['degree']
            college['program'] = i['program']
            self.graph.merge(college, "Institute", "name")
            college_location = Node('InstituteLocation')
            college_location['institute_location'] = i['location']
            self.graph.merge(college_location, "InstituteLocation", 'institute_location')
            user_education = Relationship(person, "WENT_TO", college)
            college_place = Relationship(college, "IS_IN", college_location)
            self.graph.create(user_education)
            self.graph.create(college_place)

        '''
        Data format to make graph database with required labels and properties
        '''

    def populate_job(self, job_description):
        # Make the unique job according to id
        self.graph.run('''CREATE CONSTRAINT ON (j:Job)
        ASSERT j.job_id IS UNIQUE
        ''')

        job_id = job_description['id']
        job_title = job_description['title']
        deadline = job_description['deadline']
        now = datetime.datetime.now()
        # days_remaining = deadline - now
        days_remaining = 2
        job_technical_skill = ScoreGenerator.skills_extractor(
            str(job_description['skills_qualification']) + str(job_description['requirements']) + str(
                job_description['responsibilities']))

        # job_non_skill = ['Documentation','Communication','Presentation',"Management"]
        # Create the job jod with properties
        job = Node("Job")
        job['job_id'] = job_id
        job['job_title'] = job_title
        job['deadline'] = deadline
        job['days_remaining'] = days_remaining
        self.graph.merge(job, "Job", 'job_title')

        for j in job_technical_skill:
            t_skills = Node("Technical")
            t_skills['skills'] = j
            self.graph.merge(t_skills, 'Technical', 'skills')
            job_technical_skills = Relationship(job, "REQUIRED_T_SKILL", t_skills)
            self.graph.merge(job_technical_skills)
            technical_skills = Relationship(t_skills, "IS", self.tech_skills)
            self.graph.create(technical_skills)

        # for j in job_non_skill:
        #     n_skills = Node('NonTechnical')
        #     n_skills['skills'] = j
        #     self.graph.merge(n_skills,"NonTechnical",'skills')
        #     job_non_technical_skills = Relationship(job,"REQUIRED_N_SKILL",n_skills)
        #     self.graph.merge(job_non_technical_skills)
        #     non_technical_skills = Relationship(n_skills, "IS", self.non_tech_skills)
        #     self.graph.merge(non_technical_skills)
        print("populating complete++++++=======++++++++=========")

    def create_applied_for_relation(self,job_id,employee_id):
        self.graph.run(
                        '''MATCH (p:Person{person_id:{
                                    person_applied
                                    }
                                    }
                                    ),
                                    (j:Job{job_id:{applied_job}})
                                    MERGE (p)-[:APPLIED_FOR]->(j)
                                    RETURN p,j
                                    ''', person_applied = employee_id,
                                            applied_job = job_id )

    def recommender(self, person_id):

        recommended_results = self.graph.run('''
                MATCH (p:Person) WHERE p.person_id = %d
                MATCH (j:Job)
                MATCH (p)-[:HAS_SKILL]->(t_skills)<-[:REQUIRED_T_SKILL]-(j) 
                WHERE NOT EXISTS ((p)-[:APPLIED_FOR]->(j))
                RETURN j.job_title,
                    j.job_id,
                    COUNT(*) AS skillsInCommon

                ORDER BY skillsInCommon DESC
                LIMIT 10
                ''' % (person_id), )
        recommended_records = recommended_results

        values = []
        for row in recommended_records:

            value = row.values()
            values.append(value)
        return values


class UpdateGraphUser:

    def __init__(self):
        self.graph = Graph('bolt://silver-cathryn-island-ruth.graphstory.cloud:7687', auth=('silver_cathryn_island_ruth', 'IGyKoMerrcjVwN4CeJxJ8uqLjo7Me'))
        self.tech_skills = Node("TechnicalSkills")
        self.non_tech_skills = Node("NonTechnicalSkills")

    def update_person(self,person_id, first_name, middle_name, last_name, gender):
        self.graph.run('''MERGE (p:Person{person_id: {person_id}})
                     SET p = {person_id:{person_id},first_name:{first_name},last_name:{last_name},middle_name:{middle_name},gender:{gender}}
                     RETURN p
            ''', person_id=person_id, first_name=first_name, last_name=last_name, middle_name=middle_name,
                  gender=gender)

    def update_user_technical_skill(self,person_id, technical_skill):
        self.graph.run('''MATCH (p:Person{person_id:{person_id}})-[r:HAS_SKILL]->(t:Technical)
                     DELETE r
                    ''', person_id=person_id)
        for i in technical_skill:
            t_skills = Node("Technical")
            t_skills['skills'] = i
            self.graph.merge(t_skills, 'Technical', 'skills')
            self.graph.run('''MATCH (p:Person{person_id:{person_id}}),(t:Technical{skills:{i}}) MERGE (p)-[:HAS_SKILL]->(t) 
                  ''', person_id=person_id, i=i)
            self.graph.run(''' MATCH (ts:Technical{skills:{i}}),(tss:TechnicalSkills) MERGE (ts)-[:IS]->(tss)
                  ''', i=i)


    def update_user_non_technical_skill(self,person_id, non_technical_skill):
        self.graph.run('''MATCH (p:Person{person_id:{person_id}})-[r:HAS_SKILL]->(nt:NonTechnical)
                     DELETE r
                    ''', person_id=person_id)
        for i in non_technical_skill:
            n_skills = Node("NonTechnical")
            n_skills['skills'] = i
            self.graph.merge(n_skills, 'NonTechnical', 'skills')
            self.graph.run('''MATCH (p:Person{person_id:{person_id}}),(n:NonTechnical{skills:{i}}) MERGE (p)-[:HAS_SKILL]->(n) 
                  ''', person_id=person_id, i=i)
            self.graph.run(''' MATCH (nts:NonTechnical{skills:{i}}),(ntss:NonTechnicalSkills) MERGE (nts)-[:IS]->(ntss)
                  ''', i=i)



    def update_user_work(self,person_id,exp):
        self.graph.run('''MATCH (p:Person{person_id:{person_id}})-[r:WORKED_IN]->(w:Work) WHERE  
                     DELETE r
                    ''', person_id=person_id)
                   
        
        work_details = Node('Work')
        work_details['company'] = exp['organization']
        name = exp['organization']
        work_details['designation'] = exp['job_title']
        self.graph.merge(work_details, "Work", "company")
        work_address = Node('CompanyLocation')
        work_address['company_location'] = exp['location']
        location = exp['location']
        
        self.graph.merge(work_address, "CompanyLocation", 'company_location')
        self.graph.run('''MATCH (p:Person{person_id:{person_id}}),(w:Work{company:{name}}) MERGE (p)-[:WORKED_IN]->(w) 
                  ''', person_id=person_id, name=name)
        self.graph.run('''MATCH (wk:Work{company:{name}})-[r:IS_IN]->(c:CompanyLocation)
                    DELETE r
                ''', name=name)
        self.graph.run('''MATCH (wk:Work{company:{name}}),(cl:CompanyLocation{company_location:{location}}) MERGE (wk)-[:IS_IN]->(cl) 
                  ''', name=name, location=location)


    def update_user_education(self,person_id, education):
        self.graph.run('''MATCH (p:Person{person_id:{person_id}})-[r:WENT_TO]->(i:Institute)
                     DELETE r
                    ''', person_id=person_id)
        for i in education['institutes']:
            college = Node('Institute')
            college['name'] = i['name']
            name = i['name']
            college['degree'] = i['degree']
            # college['program'] = i['program']
            self.graph.merge(college, "Institute", "name")
            college_location = Node('InstituteLocation')
            college_location['institute_location'] = i['location']
            location = i['location']
            self.graph.merge(college_location, "InstituteLocation", 'institute_location')
            # graph.merge(work_address,"CompanyLocation",'company_location')
            self.graph.run('''MATCH (p:Person{person_id:{person_id}}),(c:Institute{name:{name}}) MERGE (p)-[:WENT_TO]->(c) 
                  ''', person_id=person_id, name=name)
            self.graph.run('''MATCH (in:Institute{name:{name}})-[r:IS_IN]->(il:InstituteLocation)
                     DELETE r
                    ''', name=name)
            self.graph.run('''MATCH (ins:Institute{name:{name}}),(insl:InstituteLocation{institute_location:{location}}) MERGE (ins)-[:IS_IN]->(insl) 
                  ''', name=name, location=location)





class UpdateGraphJob:
    def __init__(self):
        self.graph = Graph('bolt://silver-cathryn-island-ruth.graphstory.cloud:7687', auth=('silver_cathryn_island_ruth', 'IGyKoMerrcjVwN4CeJxJ8uqLjo7Me'))
        self.tech_skills = Node("TechnicalSkills")
        self.non_tech_skills = Node("NonTechnicalSkills")

    def update_job(self,job_id,job):
        job_title = job.get("title")
        deadline = job.get("deadline")
        # days_remaining = deadline - datetime.datetime.now()
        days_remaining = 2
        self.graph.run('''MERGE (j:Job{job_id: {job_id}})
                     SET j = {job_id:{job_id},job_title:{job_title},deadline:{deadline},days_remaining:{days_remaining}}
                     RETURN j
            ''', job_id=job_id, job_title=job_title, deadline=deadline, days_remaining=days_remaining)

    # update_job(job_id, job_title, deadline, days_remaining)

    # Update job technical skills
    def update_job_technical_skill(self,job_id, job_description):
        job_technical_skill = ScoreGenerator.skills_extractor(
            str(job_description['skills_qualification']) + str(job_description['requirements']) + str(
                job_description['responsibilities']))

        self.graph.run('''MATCH (j:Job{job_id:{job_id}})-[r:REQUIRED_T_SKILL]->(t:Technical)
                     DELETE r
                    ''', job_id=job_id)
        for j in job_technical_skill:
            t_skills = Node("Technical")
            t_skills['skills'] = j
            self.graph.merge(t_skills, 'Technical', 'skills')
            self.graph.run('''MATCH (j:Job{job_id:{job_id}}),(t:Technical{skills:{j}}) MERGE (j)-[:REQUIRED_T_SKILL]->(t) 
                  ''', job_id=job_id, j=j)
            self.graph.run(''' MATCH (ts:Technical{skills:{j}}),(tss:TechnicalSkills) MERGE (ts)-[:IS]->(tss)
                  ''', j=j)


    # # Update job non technical skills
    # def update_job_non_technical_skill(self,job_id, job_non_skill):
    #     self.graph.run('''MATCH (j:Job{job_id:{job_id}})-[r:REQUIRED_N_SKILL]->(nt:NonTechnical)
    #                  DELETE r
    #                 ''', job_id=job_id)
    #     for j in job_non_skill:
    #         n_skills = Node("NonTechnical")
    #         n_skills['skills'] = j
    #         self.graph.merge(n_skills, 'NonTechnical', 'skills')
    #         self.graph.run('''MATCH (j:Job{job_id:{job_id}}),(nt:NonTechnical{skills:{j}}) MERGE (j)-[:REQUIRED_N_SKILL]->(nt)
    #               ''', job_id=job_id, j=j)
    #         self.graph.run(''' MATCH (nts:NonTechnical{skills:{j}}),(ntss:NonTechnicalSkills) MERGE (nts)-[:IS]->(ntss)
    #               ''', j=j)



    # Remove applied status
    def remove_applied_status(self,person_id, job_id):
        self.graph.run('''MATCH (p:Person{person_id:{person_id}})-[r:APPLIED_FOR]->(j:Job{job_id:{job_id}})
                     DELETE r
                    ''', person_id=person_id, job_id=job_id)




