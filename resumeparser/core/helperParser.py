import re
import nltk
from resumeparser.formatter.formatdata import key_value_identifier

def email_phone_extractor(text):
    '''It takes a profile segment of the resume
    and parses the email,phones etc
    uses regex for email,phone number
    :type text:string
    :returns: email,phone number
    '''
    regex_email = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"
    regex_phone = r'^[+]{0,1}[(+]{0,2}[0-9]{1,4}[)]{0,1}[-\s\.0-9]*$'
    phone = re.findall(regex_phone, text)
    email = re.findall(regex_email,text)
    if phone:
        phone_number = [num for num in phone if len(num) > 5]
    else:
        phone_number = [123456]
    return email,phone_number




def links_extractor(text):
    '''
    Function is a part of helper parser
    makes use of regex to extract the links
    like github links and linked in links
    :type text : string
    :returns: linkedin,github links
    '''

    regex_git = r"github.com/[^ |^\n]+"
    regex_linkedin = r"linkedin.com/[^ |^\n]+"
    return (re.findall(regex_git, text), re.findall(regex_linkedin, text))

def zip_code_extractor(text):
    '''
    zip_code extractor takes the whole
    text of the resume and makes the search
    of zip code within the first 25% of the resume text
    :type text: string
    :return: zipcode
    '''
    splitted_text = text.split('\n')
    len_of_text = len(splitted_text)
    new_text = ''
    i = 0
    while i < len_of_text / 4:
        new_text += ' ' + splitted_text[i] + ','
        new_text = re.sub(r'\n+', '\n', new_text)
        i += 1
    regex_zip = r'(\b\d{5}-\d{4}\b|\b\d{5}\b\s)'
    return (re.findall(regex_zip, text))

def nationality_extractor(text):
    punctuations = [':',"-",":-"]
    regex_nationality = r'[N|n]ationality\s*[:|-|:-]?\s*[^\n]*'
    try:
        nationality = nltk.word_tokenize(re.findall(regex_nationality,text)[0])
        extracted_nationality = [matched_item for matched_item in nationality
                                if matched_item.strip() not in punctuations
                                and matched_item.find('ational')== -1]
    except  Exception as e :
        print("passing the task to backup parser")
        return []

    return extracted_nationality

def key_value_parser(text):
    name,address,zip = "null","null","null"
    key_value_pair = key_value_identifier(text)
    for key,value in key_value_pair.items():
        if key=="Name":
            name = value
        elif key == "Address":
            address = value
        elif key == "Zip":
            zip = value
    return name,address,zip


def regex_parser(text,mode):
    '''
    The function here decides which
    function to call for extracting
    the personal information ,links
    and zipcode
    :type text:string
    :type mode:string
     '''
    options={"personal_info":email_phone_extractor,
             "links":links_extractor,
             "zipcode":zip_code_extractor,
             "nationality":nationality_extractor,
             "kv_parse":key_value_parser
             }

    return(options[mode](text))




