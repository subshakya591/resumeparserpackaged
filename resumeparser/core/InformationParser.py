import re

import spacy
import textacy

from resumeparser.core.helperParser import regex_parser
from resumeparser.core.namedentityrecognizer import StanfordNER
from resumeparser.core.predefinedpoolparser import language_parser,nationality_parser
from resumeparser.core.secondaryparser import SecondParser
from resumeparser.formatter.formatdata import get_ngrams


class InformationParser:

    #constructor to initialize the parser with the plain text extracted
    # the filereader and the segment from the code segmenter.
    def __init__(
            self,ner_tagger_academics, ner_tagger_personal,
            model_personal,model_academics,ner_tagger,nlp,
            non_technical_skills, technical_skills,nationality,
            information,raw_text
            ):
        '''
        Recieves the plain text
        loads the respective models
        for extracting informations
        :type ner_tagger_academics: stanfordNER tagger
        :type ner_tagger_personal : stanfordNER tagger
        :type model_personal: stanfordNER model
        :type model_academics: stanfordNER model
        :type ner_tagger: stanfordNER tagger
        :type information: string
        :type raw_text: string
        '''
        self.ner_tagger_academics=ner_tagger_academics
        self.ner_tagger_personal=ner_tagger_personal
        self.model_personal=model_personal
        self.model_academics=model_academics
        self.ner_tagger=ner_tagger
        self.resume_segment=information
        self.raw_text=raw_text
        self.tokens=[token.text for token in (nlp(raw_text))]
        self.stopwords = spacy.lang.en.stop_words.STOP_WORDS
        self.non_technical_data = non_technical_skills
        self.technical_data = technical_skills
        self.nationality_data = nationality
        self.nlp = nlp
        self.doc = textacy.make_spacy_doc(raw_text, lang='en_core_web_sm')
        self.cv_bigrams = get_ngrams(self.doc,2)
        self.cv_trigrams = get_ngrams(self.doc,3)
        self.line_index = {line:idx for idx,line in enumerate(self.raw_text.split('\n'))}

    #function that calls the regex parser as well as stanford Ner for
    #personal information extraction

    def personal_information_parser(self):
        '''
        tries to get the profile information thrown by the segmentation code
        if segment is catched passes the segment to the regex parser and stanfordNER
        parser for extracting the profile information else passes the whole raw text
        to the secondary parser
        :returns : name,address,emails,github,linkedin,nationality, zipcode
        '''
        text=self.resume_segment["profile"]
        github,linkedin=regex_parser(self.raw_text,"links")
        zipcode = regex_parser(text, "zipcode")
        #if segment is catched then follow if rule else use the secondparser for profile extraction
        if text:
            emails,phone=regex_parser(text,"personal_info")
            name,address=StanfordNER.ner_parser(self.ner_tagger_personal,text,"personal_info")
        else:
            emails,phone,name,address=SecondParser.second_personal_info_parser(self.ner_tagger_personal,self.raw_text)

        Nationality = regex_parser(self.raw_text,'nationality')
        if Nationality:
            pass
        else:
            Nationality=nationality_parser(self.tokens,self.cv_bigrams,self.nationality_data)

        r_name,r_address,r_zip = regex_parser(self.raw_text,'kv_parse')
        if r_name !="null":
            name = r_name
        if r_address !="null":
            address = r_address
        if r_zip != "null":
            zipcode = r_zip

        return name,address,emails,phone,github,linkedin,Nationality,zipcode


    #function that extracts the objective of the candidate
    def objective_parser(self):
        '''
        Just verifies whether the catched
        objective segment is actual objective
        or not
        :return : job seeker's objective (type:string)
        '''
        objectives = [self.resume_segment["objectives"]]
        return objectives


    def skills_parser(self):
        '''
        this segment calls the skill chunker of SpacyHelper
        and thus searches for those chunked phrases in the bag of skills
        :returns : list of technical skill,non-tech skills
        '''
        doc = self.nlp(self.raw_text)
        noun_chunks = [(chunk.text).lower() for chunk in list(doc.noun_chunks)]
        tokens = [token for token in self.tokens if token not in self.stopwords]
        skillset = []
        non_technical_skillset=[]
        self.technical_data['posses_skills'] = self.technical_data['Predefined_skills'].apply(lambda x:1 if x in tokens else 0)
        skillset.extend(self.technical_data.Predefined_skills.loc[self.technical_data['posses_skills']==1].values)
        self.technical_data['posses_skills'] = self.technical_data['Predefined_skills'].apply(lambda x: 1 if x in noun_chunks else 0)
        skillset.extend(self.technical_data.Predefined_skills.loc[self.technical_data['posses_skills']==1].values)
        return ([i.capitalize() for i in set([i.lower() for i in skillset])],[i.capitalize() for i in set([i.lower() for i in non_technical_skillset])])


    # function that parses the user work experience
    def experience_parser(self):
        '''
        Experience parser parses the experience
        calls the Named entity recognizer for extracting
        technical experience and experience. Makes use of
        resume segment if segment is catched else uses the
        whole resume text
        :returns : experience,technical experience

        '''
        text = self.resume_segment["experiences"]
        technical_experience=StanfordNER.ner_parser(self.ner_tagger_academics,self.raw_text,"tech_exp")
        if text:
            #if segment is found
            experience = (StanfordNER.ner_parser(self.ner_tagger_academics,text, "experience"))
            return experience,technical_experience
        else:
            # if segmenter fails to catch the exp block
            experience=(StanfordNER.ner_parser(self.ner_tagger_academics,self.raw_text,"experience"))
            return experience,technical_experience

    #parses the education
    def education_parser(self):
        '''
        Also makes use or Name Entity Recognizer.
        If education segment is catched it passes
        the academics tagger and resume segment
        but incase of failed segmentation passes
        whole resume text to the NER
        :return : academics (dict type )(contains: university,location, date,field of study)
        '''
        text=self.resume_segment["academics"]
        if text and len(text)>20:
            text = text
        else:
            text = self.raw_text
        academics = StanfordNER.ner_parser(self.ner_tagger_academics,text,"academics")
        return academics

    #language parser to parse language from the predefined pool
    def language_parser(self):
        '''
        function that uses the language_parser(text)
         function to extract the language
         :return:list of language
         '''
        language=language_parser(self.tokens)
        if language:
            return language
        else:
            return ["English"]


    # parses the project if any (simple approach)
    def project_parser(self):
        '''just returns the project segment
        from the segmented code if available'''
        text=self.resume_segment["projects"]
        if text:
            projects=text
            return projects
        else:
            return (["projects not found"])

    # parses the references (simple approach)
    def references_parser(self):
        '''just returns the references
        from the resume segment'''
        text=self.resume_segment["references"]
        if text:
            references=text
            return references
        else:
            return (["references not available"])


    def rewards_parser(self):
        '''
        just returns the rewards
        from the resume segment
        '''

        text=self.resume_segment["rewards"]
        if text:
            return text
        else:
            return(["rewards not found"])









