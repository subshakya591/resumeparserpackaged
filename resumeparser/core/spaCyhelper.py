import textacy



class SpacyHelper:
    @staticmethod
    def ngram_creator(text,n_gram):
        doc = textacy.make_spacy_doc(text,lang = 'en_core_web_sm')
        n_grams = list(textacy.extract.ngrams(doc,n_gram))
        return list(set(n_grams))

    @staticmethod
    def skill_chunker(section):
        chunked_phrases = []

        for line in section:
            pattern = r'<NOUN><ADP>?<NOUN>*[-|:]?<NUM>?'
            doc = textacy.make_spacy_doc(line,lang='en_core_web_sm')
            lists = textacy.extract.matches(doc, pattern)

            for list_item in lists:
                chunked_phrases.append(list_item.text)

        chunked_phrases = [x for x in chunked_phrases if x]
        return (list(set(chunked_phrases)))















