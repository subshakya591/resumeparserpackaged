def language_parser(resume_tokens):
    predefined_language = [
                           "english", "hindi", "nepali", "russian",
                           "sanskrit", "chinese", "japanese", "italian",
                           "spanish", "mexican", "german", "arabic",
                           "bengali", "malay", "portugese", "french", "punjabi",
                           "telgu", "tamil","newari"
                            ]
    language = ([w for w in resume_tokens if w.lower() in predefined_language])
    language= [i.capitalize() for i in set([i.lower() for i in language])]
    return list(set(language))


def nationality_parser(resume_tokens,resume_bigrams,nationality):

    nationality['matched'] = nationality['Nationality'].apply(lambda x: 1 if x in resume_bigrams else 0)
    matched_nationality = nationality.Nationality.loc[nationality['matched'] == 1].values
    matched_nationality = matched_nationality.tolist()
    if matched_nationality:
        return matched_nationality
    else:
        nationality['matched'] = nationality['Nationality'].apply(lambda x: 1 if x in resume_tokens else 0)
        matched_nationality = nationality.Nationality.loc[nationality['matched'] == 1].values
        matched_nationality = matched_nationality.tolist()
        if matched_nationality:
            return matched_nationality
        else:
            return ['US']

# def technical_skill_parser(resume_tokens,resume_bigrams,technical_skills):











