import numpy as np
from keras.preprocessing.sequence import pad_sequences


def predict_gender(loaded_model,tokenizer,name):
    text = np.array([name])
    sequences_test = tokenizer.texts_to_sequences(text)
    sequence = pad_sequences(sequences_test,maxlen = 10)
    prediction = loaded_model.predict(sequence)
    if (prediction>0.3):
        return("Male")
    else:
        return("Female")
