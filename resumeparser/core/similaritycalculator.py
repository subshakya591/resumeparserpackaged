import spacy
from scipy.spatial.distance import cosine

def sentence_similarity(sentence1,sentence2):
    nlp = spacy.load("en_core_web_sm")
    vector1= nlp(sentence1).vector
    vector2 =nlp(sentence2).vector
    score = 1-cosine(vector1,vector2)
    print("score",sentence1,score,sentence2)
    return score





