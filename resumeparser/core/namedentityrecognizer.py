import nltk

from resumeparser.formatter.dateformatter import format_date


class StanfordNER:

    ###----<NER model that chooses the particular model and parser>----###
    @staticmethod
    def ner_parser(model,text,mode):
        words = nltk.word_tokenize(text)
        parser = {
                'personal_info':StanfordNER.personal_info_parser,
                'academics':StanfordNER.education_parser,
                'experience':StanfordNER.experience_parser,
                'tech_exp':StanfordNER.tech_exp_parser
                }
        #import pdb;pdb.set_trace()
        tagged_tuples = []
        doc = model(str(words))
        for token in doc:
            tagged_tuples.append([token.text, token.ent_type_])
        #import pdb;pdb.set_trace()
        #tagged_tuples = model.tag(words)
        return parser[mode](tagged_tuples)


    @staticmethod
    def tech_exp_parser(tagged_tuples):
        tech_exp=[]
        possible_tech_exp=[]
        for word,tag in tagged_tuples:
            if tag=="EXPERIENCE":
                possible_tech_exp.append(word)
            else:
                if possible_tech_exp:
                    tech_exp.append(" ".join(possible_tech_exp))
                    possible_tech_exp=[]
        return tech_exp


    @staticmethod
    def personal_info_parser(tagged_tuples):
        name = []
        address = []
        possible_name = []
        possible_address = []
        final_address=[]
        for word, tag in tagged_tuples:
            if tag == "PERSON":
                possible_name.append(word)
            else:
                if possible_name:
                    name.append(" ".join(possible_name))
                    possible_name = []

        for word, tag in tagged_tuples:
            if tag == "LOCATION" and word not in possible_address:
                possible_address.append(word)
            else:
                if possible_address:
                    address.append(" ".join(possible_address))
                    possible_address = []

        for ad in address:
            if ad not in final_address:
                final_address.append(ad)
            else:
                pass
        return name, final_address

    @staticmethod
    def education_parser(tagged_tuples):
        possible_university=[]
        possible_degree=[]
        possible_date=[]
        possible_location=[]
        university_name={}
        university_degree={}
        university_loc={}
        date={}
        deg_idx=0
        date_idx=0
        loc_idx=0
        uni_idx=0
        academics={}
        for word,tag in tagged_tuples:
            if tag=="DEGREE":
                possible_degree.append(word)

            else:
                if possible_degree:
                    university_degree.update({" ".join(possible_degree):deg_idx})
                    deg_idx+=1

                    possible_degree=[]

        if possible_degree:
            university_degree.update({" ".join(possible_degree): deg_idx})
            deg_idx += 1


        for word,tag in tagged_tuples:
            if tag=="DATE":
                possible_date.append(word)
            else:
                if possible_date:
                    date.update({" ".join(possible_date):date_idx})
                    date_idx+=1
                    possible_date = []
        if possible_date:
            date.update({" ".join(possible_date):date_idx})
            date_idx+=1

        for word,tag in tagged_tuples:
            if tag=="UNIVERSITY":
                possible_university.append(word)
            else:
                if possible_university:
                    university_name.update({" ".join(possible_university):uni_idx})
                    uni_idx+=1
                    possible_university=[]
        if possible_university:
            university_name.update({" ".join(possible_university):uni_idx})

        for word,tag in tagged_tuples:
            if tag=="LOCATION":
                possible_location.append(word)
            else:
                if possible_location:
                    university_loc.update({" ".join(possible_location):loc_idx})
                    loc_idx+=1
                    possible_location=[]
            if possible_location:
                university_loc.update({" ".join(possible_location):loc_idx})


        x=1
        for key,value in university_degree.items():
            academics.update({"E{}".format(x):{"Degree":key}})
            for ky,val in university_name.items():
                if value==val:
                    academics["E{}".format(x)].update({"university":ky})
            for lky,lval in university_loc.items():
                if value==lval:
                    academics["E{}".format(x)].update({"location":lky})
            for dky,dval in date.items():
                dky=format_date(dky)
                if len(university_degree)==len(date) and value==dval:
                    academics["E{}".format(x)].update({"graduated_year":dky})
                    break
                else:
                    enrolled_date={}
                    exit_date={}
                    i=0
                    en_count=0
                    ex_count=0
                    for k,v in date.items():
                        if i%2==0:
                            enrolled_date.update({k:en_count})
                            i+=1
                            en_count+=1
                        else:
                            exit_date.update({k:ex_count})
                            ex_count+=1
                            i+=1
                            for ek,ev in enrolled_date.items():
                                ek=format_date(ek)
                                if value==ev:
                                    academics["E{}".format(x)].update({"enrolled_date":ek})
                            for gk,gv in exit_date.items():
                                gk=format_date(gk)
                                if value==gv:
                                    academics["E{}".format(x)].update({"graduated_date":gk})

            x+=1
        return(academics)

    @staticmethod
    def experience_parser(tagged_tuples):
        possible_location=[]
        possible_company = []
        possible_designation = []
        possible_date = []
        locations={}
        ex_company_name = {}
        designation = {}
        date = {}
        deg_idx = 0
        date_idx = 0
        com_idx = 0
        loc_idx=0
        experience = {}

        for word, tag in tagged_tuples:
            if tag == "DESIGNATION":
                possible_designation.append(word)
            else:
                if possible_designation:
                    designation.update({" ".join(possible_designation): deg_idx})
                    deg_idx += 1
                    possible_designation = []

        for word, tag in tagged_tuples:
            if tag == "DATE":
                possible_date.append(word)
            else:
                if possible_date:
                    date.update({" ".join(possible_date): date_idx})
                    date_idx += 1
                    possible_date = []
        if possible_date:
            date.update({" ".join(possible_date): date_idx})
            date_idx += 1

        for word, tag in tagged_tuples:
            if tag == "COMPANY":
                possible_company.append(word)
            else:
                if possible_company:
                    ex_company_name.update({" ".join(possible_company): com_idx})
                    com_idx += 1
                    possible_company = []
        if possible_company:
            ex_company_name.update({" ".join(possible_company): date_idx})

        for word, tag in tagged_tuples:
            if tag == "LOCATION":
                possible_location.append(word)
            else:
                if possible_location:
                    locations.update({" ".join(possible_location): loc_idx})
                    loc_idx += 1
                    possible_location = []
        if possible_location:
            locations.update({" ".join(possible_location): loc_idx})

        x = 1
        for key, value in designation.items():
            experience.update({"Exp{}".format(x): {"Designation": key}})
            for ky, val in ex_company_name.items():
                if value == val:
                    experience["Exp{}".format(x)].update({"company": ky})
            for lky,lval in locations.items():
                if value==lval:
                    experience["Exp{}".format(x)].update({"location":lky})
            for dky, dval in date.items():
                dky=format_date(dky)
                if len(designation) == len(date) and value == dval:
                    experience["Exp{}".format(x)].update({"exit year": dky})
                    break
                else:
                    entry_date = {}
                    exit_date = {}
                    i = 0
                    en_count = 0
                    ex_count = 0
                    for k, v in date.items():
                        if i % 2 == 0:
                            entry_date.update({k: en_count})
                            i += 1
                            en_count += 1
                        else:
                            exit_date.update({k: ex_count})
                            ex_count += 1
                            i += 1
                            for ek,ev in entry_date.items():
                                ek=format_date(ek)
                                if value == ev:
                                    experience["Exp{}".format(x)].update({"entry_date": ek})
                            for gk, gv in exit_date.items():
                                gk=format_date(gk)
                                if value == gv:
                                    experience["Exp{}".format(x)].update({"exit_date": gk})

            x += 1
        print(experience)
        return (experience)







