import re

from resumeparser.core.helperParser import regex_parser
from resumeparser.core.namedentityrecognizer import StanfordNER

class SecondParser:

    @staticmethod
    def second_personal_info_parser(model,text):
        splitted_text=text.split('\n')
        len_of_text=len(splitted_text)
        new_text=''
        i=0
        while i<len_of_text/4:
            new_text+='\n'+ splitted_text[i] +','
            new_text=re.sub(r'\n+','\n',new_text)
            i+=1
        emails,phone=regex_parser(new_text,"personal_info")
        name,address= StanfordNER.ner_parser(model,new_text,"personal_info")
        return emails,phone,name,address
