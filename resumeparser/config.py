import os


ROOT = os.path.dirname(os.path.abspath(__file__))
BASENAME = os.path.basename(ROOT)
RESUMEPARSER = os.path.join(ROOT, 'resumeparser')
