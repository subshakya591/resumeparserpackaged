import datetime
import re
from dateutil.parser import parse
from nltk.tokenize import word_tokenize

#function that takes date in any string form and formats it into the uniform format
def format_date(date):
    date=re.sub(r'-',' -',date)
    date=re.sub(r'-','- ',date)
    try:
        formatted_date=parse(date).strftime("%Y-%m-%d")
        return formatted_date

    except:
        possible_date=''
        tokens=word_tokenize(date)
        for token in tokens:
            if token.isdigit():
                possible_date+=token+" "
        try:
            formatted_date=parse(possible_date).strftime(("%Y-%m-%d"))
        except:
            dates=[]
            possible_date=' '
            for character in date:
                if character.isdigit():
                    possible_date+=character
                else:
                    dates.append(possible_date)
                    possible_date=''
                    pass
            try:
                formatted_date=parse(possible_date).strftime("%Y-%m-%d")
            except:
                formatted_date=(datetime.datetime.now()).strftime("%Y-%m-%d")

    return (formatted_date)


def comparedates(*args):
    '''
    function that compares the dates and return the dictionary
    dates with large and small in the key
    :type *args string
    '''
    dates = [arg for arg in args]
    return {"Large date": max(dates), "Small date": min(dates)}




