import os
import re

from subprocess import Popen,PIPE
# import Image
from pytesseract import image_to_string
import docx2txt
from PIL import Image
from tika import parser

from resumeparser.formatter.regularExpression import formatDateInString


def clean_text(text):
    text = re.sub(r' +', ' ', text)
    text = re.sub(r'\t', ' \n, ', text)
    text = re.sub(r'\n+', ' \n ,', text)
    text = re.sub(r',+', ',', text)
    text = text.encode('ascii', errors='ignore').decode("utf-8")
    text = formatDateInString(text)
    print (text)
    return text

def pdf_to_text(filepath):
    pdf_file = parser.from_file(filepath)

    text = pdf_file["content"]
    text = clean_text(text)
    print("Cleaned text",text)
    return text


def docx_to_text(filepath):
    text = ""
    text += docx2txt.process(filepath)
    text = clean_text(text)
    return text

def txt_to_text(filepath):
    text = ""
    with open(filepath, mode='r', encoding='unicode_escape', errors='strict', buffering=1) as file:
        data = file.read()
    text += data
    text = clean_text(text)
    return text

def doc_to_text(filepath):
    text = ""
    cmd = ['antiword', filepath]
    p = Popen(cmd, stdout=PIPE)
    stdout, stderr = p.communicate()
    text += stdout.decode('utf-8', 'ignore')
    text = clean_text(text)
    return text

def img_to_text(filepath):
    text = image_to_string(Image.open('test.png'))
    text = clean_text(text)
    return text

def read_cv(file_path):
    image_extensions=['.jpeg','.png','.jpg','.psd','.ai']
    _,file_extension=(os.path.splitext(file_path))
    if file_extension.lower() in image_extensions:
            file_extension='.img'
    else:
        pass

    options={
            '.pdf':pdf_to_text,
            '.docx':docx_to_text,
            '.txt':txt_to_text,
            '.doc':doc_to_text,
            '.img':img_to_text,
            }
    try:
        text=options[file_extension](file_path)
        return text
    except Exception as e:
        print("Exception at fileReader:"+str(e))
