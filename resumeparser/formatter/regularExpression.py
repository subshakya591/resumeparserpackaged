import re


# converts individual date in array to formated date separated with -
def getFormatedDate(date, type):
    returnValue = ""
    if type == "three":
        partsOfDate = re.search(r'(\d{1,4})[.\/-](\d{1,2})[.\/-](\d{1,4})', date)
        returnValue = partsOfDate.group(1) + '-' + partsOfDate.group(2) + '-' + partsOfDate.group(3)
    elif type == "two":
        partsOfDate = re.search(r'(\d{1,4})[.\/-](\d{1,4})', date)
        returnValue = partsOfDate.group(1) + '-' + partsOfDate.group(2)
    return returnValue


# get all dates in passes string
def getUnformatedDates(stringWithDate):
    matchedObjects3 = re.findall(r'(\d{1,4}[.\/-]\d{1,2}[.\/-]\d{1,4})', stringWithDate, re.M | re.I)
    matchedObjects2 = re.findall(r'(\d{1,4}[.\/-]\d{1,4})', stringWithDate, re.M | re.I)
    returnValue = {"twoParts": [], "threeParts": []}
    if matchedObjects3:
        returnValue["twoParts"] = matchedObjects2
        returnValue["threeParts"] = matchedObjects3
    else:
        print("No match!!")
    return returnValue


# main method where following steps are performed :
# 1. find date like construct in passed string
# 2. convert each date to uniform pattern
# 3. replace unformated date to formated date in original string
def formatDateInString(stringToFormat):
    dateListUnformated = getUnformatedDates(stringToFormat)
    dateListFormatedTwo = []
    dateListFormatedThree = []
    for i in dateListUnformated["twoParts"]:
        dateListFormatedTwo.append(getFormatedDate(i, "two"))
    for i in dateListUnformated["threeParts"]:
        dateListFormatedThree.append(getFormatedDate(i, "three"))
    formatedString = ""
    for idx, val in enumerate(dateListFormatedThree):
        if (idx == 0):
            formatedString = stringToFormat.replace(dateListUnformated["threeParts"][idx], dateListFormatedThree[idx])
        else:
            formatedString = formatedString.replace(dateListUnformated["threeParts"][idx], dateListFormatedThree[idx])
    for idx, val in enumerate(dateListFormatedTwo):
        formatedString = formatedString.replace(dateListUnformated["twoParts"][idx], dateListFormatedTwo[idx], 1)
    return formatedString
# stringToFormat = "whole text may contain some 06.2019 10/2082 12.05 2070/13 2070.05.31 dates like 06.09.2019 and some 2019/8/7 sample date 2015-02/15 "
# formatedString = formatDateInString(stringToFormat);  # this is formated string
#
# print(stringToFormat)
# print(formatedString)
