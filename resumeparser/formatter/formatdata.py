import re
import textacy

def key_value_formatter(list_kv):
    syn_name = ['Name', 'name']
    syn_birthdate = ['D.O.B', "Date of Birth", "Birthdate", "Birth date", "D-O-B"]
    syn_address = ['Address', 'Location', 'address']
    rule = r'[^:|\s|\n|-]+[A-Za-z0-9\s/.,@-]+'
    key_value_pair = {}
    name_value = []
    date_value = []
    location_value = []
    for token in list_kv:
        result = re.findall(rule, token)
        if len(result) == 2:
            key = result[0]
            if key in syn_name:
                name_value.append(result[1])
                key_value_pair.update({key: name_value})
            elif key in syn_birthdate:
                key = "D.O.B"
                date_value.append(result[1])
                key_value_pair.update({key: date_value})
            elif key in syn_address:
                key = "Address"
                location_value.append(result[1])
                key_value_pair.update({key: location_value})
    return key_value_pair


def key_value_identifier(text):
    rule = r'[A-Za-z\.]+[:|-|:-][^\n]+'
    result = re.findall(rule, text)
    result = key_value_formatter(result)
    return result


def get_ngrams(doc, n):
    n_grams = list(textacy.extract.ngrams(doc, n))
    formatted_nrams = [ngm.text for ngm in n_grams]
    return formatted_nrams