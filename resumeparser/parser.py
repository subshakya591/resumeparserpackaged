from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

import pickle
import spacy
import pandas as pd
import numpy as np

from resumeparser.formatter.filereader import read_cv
from resumeparser.core.segmentcreator import ResumeSegmentCreator
from resumeparser.core.InformationParser import InformationParser
from resumeparser.core.genderpredict import predict_gender

from resumeparser import config
from common import config as common_config

class Parser:

    def __init__(self):

        self.nationality_data = pd.read_csv(os.path.join(config.ROOT,'staticfiles/nationality_data.csv'))
        self.technical_skills = pd.read_csv(os.path.join(config.ROOT,'staticfiles/technical_skills.csv'))
        self.non_technical_skills = pd.read_csv(os.path.join(config.ROOT,'staticfiles/non_technical_skills.csv'))
        with open(os.path.join(config.ROOT, 'models/segment_identifier.pkl'), 'rb')as pkl:
            self.model = pickle.load(pkl)
        with open(os.path.join(config.ROOT,'models/male_female_tokenizer.pickle'), 'rb')as handle:
            self.gender_tokenizer = pickle.load(handle)
        with open(os.path.join(config.ROOT, 'models/male_female_identifier.pickle'), 'rb') as handle:
            self.gender_model = pickle.load(handle)
        with open(os.path.join(config.ROOT, "models/resume_identifier_new.pickle"), "rb") as infile:
            self.identifier_model = pickle.load(infile)
        #jar = os.path.join(common_config.ROOT, 'stanford-ner-tagger/stanford-ner.jar')
        # model_nltk = os.path.join(common_config.ROOT, 'stanford-ner-tagger/resume.ser.gz')
        # self.ner_tagger = StanfordNERTagger(model_nltk, jar, encoding='utf8')
        # self.model_academics = os.path.join(common_config.ROOT, 'stanford-ner-tagger/resume.ser.gz')
        # self.model_personal = os.path.join(common_config.ROOT, 'stanford-ner-tagger/english.all.3class.distsim.crf.ser.gz')
        # self.ner_tagger_personal = StanfordNERTagger(self.model_personal, jar, encoding='utf8')
        # self.ner_tagger_academics = StanfordNERTagger(self.model_academics, jar, encoding='utf8')
        model_spacy = os.path.join(common_config.ROOT,'spacyNER')
        self.model_academics = os.path.join(common_config.ROOT,'spacyNER')
        self.model_personal = 'en_core_web_sm'
        self.ner_tagger = spacy.load(model_spacy)
        self.ner_tagger_personal = spacy.load(self.model_personal)
        self.ner_tagger_academics = spacy.load(model_spacy)
        self.nlp = spacy.load("en_core_web_sm")


    def resume_verifier(self,cv_content):
        status={
                1:True,
                0:False
                }
        # for spacy NER
        doc = self.ner_tagger(cv_content)
        word_pos = []
        NER_list = []
        for token in doc:
            if token.is_space != True:
                if token.ent_type == 0:
                    token.ent_type_ = 'O'
                t = [(token.text, token.ent_type_)]
                r = token.ent_type_
                word_pos.append(t)
                NER_list.append(r)
        #import pdb;pdb.set_trace()
        # for NLTK NER tagger
        # doc = self.nlp(cv_content)
        # resume_word = [token.text for token in doc]
        # word_pos = self.ner_tagger.tag(resume_word)
        # NER_list = [pos for word, pos in word_pos]
        #import pdb;pdb.set_trace()
        present_NER = set(NER_list)
        resume_features = [
                           int("TITLE" in present_NER),
                           int("PERSON" in present_NER),
                           int("LOCATION" in present_NER),
                           int("UNIVERSITY" in present_NER),
                           int("DEGREE" in present_NER),
                           int("DATE" in present_NER),
                           int("DESIGNATION" in present_NER),
                           int("COMPANY" in present_NER),
                           int("EXPERIENCE" in present_NER),
                        ]

        resume_features = np.array([resume_features])
        # import pdb;pdb.set_trace()
        result = self.identifier_model.predict(resume_features)[0]
        return status[result]


    def parse(self, cv_content):
        resume_segment = ResumeSegmentCreator(self.model, cv_content)
        segment=resume_segment.format_segment()
        resume_parser = InformationParser(
                                           self.ner_tagger_academics,
                                           self.ner_tagger_personal,
                                           self.model_personal,
                                           self.model_academics ,
                                           self.ner_tagger,
                                           self.nlp,
                                           self.non_technical_skills,
                                           self.technical_skills,
                                           self.nationality_data,
                                           segment,
                                           cv_content
                                           )
        name,address,emails,phone,\
        github,linkedin,Nationality,\
        zipcode = resume_parser.personal_information_parser()

        if name:
            first_name = name[0].split()[0]
            gender = predict_gender(self.gender_model,self.gender_tokenizer,first_name)
        else:
            first_name = "Anonymous"
            gender = "Male"

        objective = resume_parser.objective_parser()

        skill,non_tech_skills = resume_parser.skills_parser()

        academics = resume_parser.education_parser()

        rewards = resume_parser.rewards_parser()

        language = resume_parser.language_parser()

        references = resume_parser.references_parser()

        projects = resume_parser.project_parser()

        experience,technical_experience = resume_parser.experience_parser()

        if name:
            tokenized_name=name[0].strip().split(' ')
            first_name=tokenized_name[0]
            last_name=tokenized_name[-1]

            if len(tokenized_name)==3:
                middle_name=tokenized_name[1]
            else:
                middle_name="None"
        else:
            first_name="Anonymous"
            middle_name="Anonymous"
            last_name="Anonymous"


        #provides the output in the json format

        output=({
                'PERSONAL_INFORMATION':
                    {
                        'First_Name': '{}'.format(first_name),
                        'Middle_name':'{}'.format(middle_name),
                        'Last_Name': '{}'.format(last_name),
                        'Gender':gender,
                        'Email': emails,
                        'Phone_Number': phone,
                        'Address':address,
                        'Zip_Code':zipcode,
                        'github':github,
                        'linkedin':linkedin,
                        'Nationality':Nationality
                    },
                'OBJECTIVE': objective,
                'SKILLS':
                    {
                    'Skills': skill,
                    'Soft_skills':non_tech_skills
                    },
                'EXPERIENCE':experience,
                'TECHEXP':technical_experience,
                'EDUCATION':academics,
                'PROJECTS':projects,
                'REWARDS':rewards,
                'LANGUAGES': language,
                'REFERENCES': references


            }
        )

        return output

    def resumeParser(self,file_path):
        cv_content = read_cv(file_path)
        decision=self.resume_verifier(cv_content)
        if decision==True:
            output=self.parse(cv_content)
            status=True
        else:
            output="Not a resume"
            status=False
        return(output,status)



